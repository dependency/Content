//
//  Content.swift
//  Content
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import Foundation
import Utility
import Trig

public func generate() -> Int {
    return 42
}

public func generateTrig() -> Int {
    return sum(30, 20)
}

public func generateDescription() -> String {
    return describe(generate())
}
